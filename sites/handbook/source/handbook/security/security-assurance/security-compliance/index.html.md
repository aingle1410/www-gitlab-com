---
layout: handbook-page-toc
title: "Security Compliance, Commercial Team Page"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## <i class="fas fa-bullseye" style="color:rgb(110,73,203)" aria-hidden="true"></i> Security Compliance Mission

Security Compliance (Commercial) Mission:

1. Enable GitLab to be the most trusted DevSecOps offering on the market, demonstrated by security certifications and attestations.
2. Achieve, maintain and grow industry specific [security certifications and attestations](https://about.gitlab.com/handbook/security/security-assurance/security-compliance/certifications.html) for GitLab.com
3. Identify and mitigate GitLab information security risk through [continuous control monitoring](https://about.gitlab.com/handbook/security/security-assurance/security-compliance/sec-controls.html) of the GitLab.com SaaS offering and key in-scope auxiliary applications and third party sub-processors.
4. Enable security to scale through the discovery and application of compliance automation.
5. Identify and remediate observations to reduce risk and ensure continued maintenance of security certifications and attestations.
6. Work across industries and verticals to support GitLab customers in their own compliance journey.

## Core Competencies

1. [Third Party Security Certifications](https://about.gitlab.com/handbook/security/security-assurance/security-compliance/certifications.html)
   * Gap Analysis Program: feasibility for external certifiction expansion
   * External Audit coordination and hosting
   * Security Attestations
2. [Observation and Remediation Management](https://about.gitlab.com/handbook/security/security-assurance/observation-management-procedure.html)
   * Specific to Tier 3 observations
   * Identify control weaknesses and gaps (observations)
   * Provide remediation recommendations and guidance
   * Track remediation to completion
3. [Continuous Control Monitoring](https://about.gitlab.com/handbook/security/security-assurance/security-compliance/sec-controls.html) of the GitLab Control Framework
   * Compliance [production readiness assessment](https://about.gitlab.com/handbook/security/security-assurance/production_readiness_compliance_assessment.html)
   * User Access Review Program
   * Business Continuity Plan (BCP) testing
   * Information System Continuity Plan (ISCP) testing
4. Compliance Automation discovery and implementation
   * Utilizing dogfooding and external tools to continue driving compliance by default features within the product and true CCM efforts

## Where we work

We primarily work out of the [Team-Commercial Compliance group project](https://gitlab.com/gitlab-com/gl-security/security-assurance/team-commercial-compliance). This group includes subgroups and projects for:

    * Team information and directory
    * External Certifications
    * User Access Review Program
    * Audit Reports (output of CCM efforts)
    * Gap Analysis Program
    * ISCP and BCP tests and final reports
    * IT General Control Support (ITGC)

Work that overlaps with other teams including Dedicated Compliance can be found in the [Security Compliance - All Teams](https://gitlab.com/gitlab-com/gl-security/security-assurance/security-compliance-commercial-and-dedicated) group. This group includes subgroups and projects for:

    * GitLab Control Framework (GCF)
    * Observation Management
    * Security Compliance Intake (production readiness)
    * Third Party Penetration Testing Program
    * Exceptions

We also utilize external tooling including:

    * ZenGRC: control testing and observations
    * Authomize: User access review campaigns

## How we work

We strive for transparency whenever possible through the use of non confidential issues within our group projects and handbook documentation. However, not all of our work is externally visible. In order to continue striving for transparency, we are committed to delivering value to our external customers through community outreach efforts such as blogs, keeping the handbook up to date and providing documentation that demonstrates how we dogfood to meet our security compliance core compentencies.

We utilize GitLab Epics and Issue to track projects, deliverables and milestones. We are currently working on upleveing our internal metrics and reporting through the use of insights charts, issue tasks and automation.

## Metrics and Measures of Success

1. [Security Control Risk by System](https://about.gitlab.com/handbook/security/performance-indicators/#security-control-risk-by-system)
2. [Securty Observations](https://about.gitlab.com/handbook/security/performance-indicators/#security-observations-tier-3-risks)

## Contact the Team

|  Program | DRI | Responsibilities |
| :---: | :---: | :---: |
| Security Compliance (Commercial) Team manager | [@lcoleman](https://gitlab.com/lcoleman) | Establish direction, roadmap and oversight of the team core competencies and owned programs |
| External Certifications | [@madlake](https://gitlab.com/madlake) | External Audit coordination and execution for existing certifications (SOC 2 Type 2, ISO 27001, 27017, 27018) |
|  Observations | [@madlake](https://gitlab.com/madlake) | Observation Program management and metrics including observation validation, remediation recommendations and progress reporting |
|  User Access Reviews | [@alexfrank09](https://gitlab.com/alexfrank09) | Oversight of [UAR Program/ Automated UAR Tool](https://about.gitlab.com/handbook/security/security-assurance/security-compliance/access-reviews.html) including launching UAR campaigns, identifying access changes and removals, campaign ownership, metrics and reporting |
|  [Gap Analysis Program](https://about.gitlab.com/handbook/security/security-assurance/security-compliance/gap-analysis-program.html) | [@DanEckhardt](https://gitlab.com/DanEckhardt) | Oversight of Gap Analysis program and procedures, prioritize gap analysis requests, gap analysis status tracking and reporting |
|  GitLab Control Framework | [@davoudtu](https://gitlab.com/davoudtu) | Ongoing GCF review and refinement of applicable controls based on certifications and CCM coverage |
|  BCP and ISCP | [@byronboots](https://gitlab.com/byronboots) | DRI for driving and documenting BCP and ISCP activities and remediation |
|  CCM Automation | [@byronboots](https://gitlab.com/byronboots) | Stable counterpart for identifying, defining and driving automation activities for continuous control monitoring program |

## <i class="fas fa-id-card" style="color:rgb(110,73,203)" aria-hidden="true"></i> Contact the Team

* Slack
   * Feel free to tag us with `@commerical_compliance`
   * The #sec-assurance slack channel is the best place for questions relating to our team (please add the above tag)
* Tag us in GitLab
   * `@gitlab-com/gl-security/security-assurance/team-commercial-compliance`
* Email
   * `security-compliance@gitlab.com`
* [Commercial Compliance team project](https://gitlab.com/gitlab-com/gl-security/security-assurance/team-commercial-compliance/compliance)

* Interested in joining our team? Check out more [here](https://handbook.gitlab.com/job-families/security/security-compliance/)!

## <i class="fas fa-book" style="color:rgb(110,73,203)" aria-hidden="true"></i> References

* [Security Certifications](/handbook/security/security-assurance/security-compliance/certifications.html)
* [GCF Security Control Lifecycle](/handbook/security/security-assurance/security-compliance/security-control-lifecycle.html)
* [GCF Security Controls](/handbook/security/security-assurance/security-compliance/sec-controls.html)
* [User Access Reviews](/handbook/security/security-assurance/security-compliance/access-reviews.html)
* [Observation Methodology](/handbook/security/security-assurance/observation-management-procedure.html)
* [Gap Analysis Program](/handbook/security/security-assurance/security-compliance/gap-analysis-program.html)

<div class="flex-row" markdown="0" style="height:40px">
    <a href="https://about.gitlab.com/handbook/security/security-assurance/#" class="btn btn-purple-inv" style="width:100%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Return to the Security Assurance Homepage</a>
</div> 
